const express = require('express');
const app = express();
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
require('dotenv/config');
app.use(bodyParser.json());

//Import Routes
const artistsRoute = require('./routes/artists');
app.use('/artists', artistsRoute);

//Routes
app.get('/', (req,res) => {
    res.send('testing server');
});

mongoose.connect (
process.env.DB_CONNECTION,{ 
    useNewUrlParser: true
    }, () => console.log('connected to DB!')
);

//How to we start Listening the server
app.listen(3000);