const express = require('express');
const router = express.Router();
const Artist = require('../models/Artists');

// get all artist's data
router.get('/', async (req, res) => {
    try{
         const artists = await Artist.find();
         res.json(artists);
    }catch(err){
        res.json({message: err});
    }
});

module.exports = router;